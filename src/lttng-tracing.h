#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER lttng_tracing

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./lttng-tracing.h"

#if !defined(_LTTNG_TRACING_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _LTTNG_TRACING_H

#include <lttng/tracepoint.h>


/*    ****************************************************     zstr.c class */

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zstr_s_send_event,
        TP_ARGS(
        char*, address,
        char*, string
),
        TP_FIELDS(
        ctf_string(sender_address, address)
        ctf_string(string_send_to_socket, string)
)
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zstr_send_event,
        TP_ARGS(
        char*, string2
),
        TP_FIELDS(
        ctf_string(string_send_to_socket, string2)
)
)



/*    ****************************************************     zmsg.c class */

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zmsg_new,
        TP_ARGS(
                char*, content
        ),
        TP_FIELDS(
                ctf_string(message, content)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zmsg_destroy,
        TP_ARGS(
                char*, content
        ),
        TP_FIELDS(
                ctf_string(message, content)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zmsg_recv,
        TP_ARGS(
                int, routingid
        ),
        TP_FIELDS(
                ctf_string(routing_id, routingid)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zstr_recv_event,
        TP_ARGS(
        char*, address,
        char*, string
),
        TP_FIELDS(
        ctf_string(sender_address, address)
        ctf_string(string_recv, string)
)
)

/*    ****************************************************     zframe.c class */


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zframe_new,
        TP_ARGS(
                const char*, stringd
        ),
        TP_FIELDS(
                ctf_string(string_recv, stringd)
        )
)

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zframe_recv,
        TP_ARGS(
                char*, stringd
        ),
        TP_FIELDS(
                ctf_string(string_recv, stringd)
        )
)

/*    ****************************************************     zsock.c class */

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zsock_new,
        TP_ARGS(
                int, type
        ),
        TP_FIELDS(
                ctf_string(msg_type, type)
        )
)

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zsock_destroy,
        TP_ARGS(
                char*, mesg
        ),
        TP_FIELDS(
                ctf_string(message_destroyed, mesg)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_sock_new_sub_event,
        TP_ARGS(
                const char*, endpoints
        ),
        TP_FIELDS(
                ctf_string(new_bind_adress, endpoints)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_sock_new_push_event,
        TP_ARGS(
                const char*, endpoints
        ),
        TP_FIELDS(
                ctf_string(new_push_adress, endpoints)
        )
)

TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_sock_new_pull_event,
        TP_ARGS(
                const char*, endpoints
        ),
        TP_FIELDS(
                ctf_string(new_pull_adress, endpoints)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_sock_new_server_event,
        TP_ARGS(
                const char*, endpoints
        ),
        TP_FIELDS(
                ctf_string(new_server_adress, endpoints)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_sock_new_client_event,
        TP_ARGS(
                const char*, endpoints
        ),
        TP_FIELDS(
                ctf_string(new_client_adress, endpoints)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_sock_new_stream_event,
        TP_ARGS(
                const char*, endpoints
        ),
        TP_FIELDS(
                ctf_string(new_stream_adress, endpoints)
        )
)


TRACEPOINT_EVENT(
        lttng_tracing,
        zmq_zsock_bind_event,
        TP_ARGS(
                const char*, endpoint
        ),
        TP_FIELDS(
                ctf_string(new_bind_adress, endpoint)
        )
)








#endif
#include <lttng/tracepoint-event.h>